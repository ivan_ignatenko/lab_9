import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 2*.Класс StreamApiDemo, производный от LambdaDemo. Содержит следующие, использующие Stream API, лямбда-выражения:
 */
public class StreamApiDemo extends LambdaDemo {
    /**
     *1.В списке объектов удаляет все значения null
     */
    public static Function<List<Object>, List<Object>> deleteNull = x ->{
        return x.stream().filter(Objects::nonNull).collect(Collectors.toList());
    };

    /**
     *2. Во множестве целых чисел находит количество положительных значений
     */
    public static Function<Set<Integer>, Long> countOfPositive = x ->{
        if(x == null){return null;}
        return x.stream().filter(z -> z > 0).count();
    };

    /**
     *в списке объектов получить последние три элемента
     */
    public static Function<List<Object>, List<Object>> threeLastElems = x ->{
        if (x == null || x.size() < 3){return null;}
        return x.stream().skip(x.size()-3).collect(Collectors.toList());
    };

    /**
     *в списке целых чисел получить первое четное число или значение null, если в списке
     * нет четных чисел
     */
    public static Function<List<Integer>, Integer> firstEvenNum = x ->{
        Optional<Integer> res = x.stream().filter(z -> z % 2 == 0).findFirst();
        return res.orElse(null);
    };

    /**
     *по массиву целых чисел построить список квадратов элементов массива без
     * повторений
     */
    public static Function<Integer[], List<Integer>> squareOfNumber = x ->{
        if(x == null){return null;}
        return Arrays.stream(x).map(s -> s * s).distinct().collect(Collectors.toList());
    };

    /**
     *по списку строк построить новый список, содержащий все непустые строки исходного
     * списка, упорядоченные по возрастанию,
     */
    public static Function<List<String>, List<String>> sortedNonNullStrings = x->{
        return x.stream().filter(s -> s!= null && !s.isEmpty()).sorted().collect(Collectors.toList());
    };

    /**
     *множество строк превратить в список, упорядоченный по убыванию
     */
    public static Function<Set<String>, List<String>> descendingStringSort =
            x -> x.stream().filter(s -> s!= null && !s.isEmpty()).sorted(Comparator.reverseOrder()).
                    collect(Collectors.toList());

    /**
     *для множества целых чисел вычислить сумму квадратов его элементов
     */
    public static Function<Set<Integer>, Integer> sumOfNumbersSquares = x ->{
        return x.stream().reduce(0, (res, s) -> res + s*s);
    };

    /**
     *в коллекции людей вычислите максимальный возраст человека
     */
    public static Function<Collection<Human>, Integer> maxAge = x -> {
        if(x == null || x.size() == 0){return null;}
        return x.stream().max(Comparator.comparingInt(Human::getAge)).get().getAge();
    };

    /**
     *отсортируйте коллекцию людей сперва по полу, затем — по возрасту.
     */
    public static Function<Collection<Human>, Collection<Human>> genderAndAgeSorted = x -> x.stream().
            sorted(Comparator.comparingInt(Human::getAge)).sorted(new GenderComparator()).collect(Collectors.toList());


}

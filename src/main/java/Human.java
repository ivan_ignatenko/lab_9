import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * 1.Класс Human с полями фамилия, имя, отчество, возраст и пол (перечисление).
 */

public class Human {
    public enum Gender {M, F};
    private String lastName;
    private String name;
    private String surname;
    private int age;
    private Gender gender;

    public Human(String lastName, String name, String surname, int age, Gender gender) throws HumanException{
        if(lastName == null || lastName.isEmpty() || name == null || name.isEmpty() || surname == null || surname.isEmpty()){
            throw new HumanException("Empty or null name/last name/surname");
        }
        else if(age <= 0){
            throw new HumanException("Incorrect age");
        }
        this.lastName = lastName;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
    }

    @Contract(pure = true)
    public Human(@NotNull Human other) throws HumanException {
        this(other.getLastName(), other.name, other.getSurname(), other.getAge(), other.getGender());
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) throws HumanException {
        if(lastName == null || lastName.isEmpty()){
            throw new HumanException("Null or empty last name");
        }
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws HumanException {
        if(name == null || name.isEmpty()){
            throw new HumanException("Null or empty last name");
        }
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) throws HumanException {
        if(surname == null || surname.isEmpty()){
            throw new HumanException("Null or empty surname");
        }
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws HumanException {
        if(age <= 0){
            throw new HumanException("Incorrect age");
        }
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        return age == human.age && Objects.equals(lastName, human.lastName) && Objects.equals(name, human.name)
                && Objects.equals(surname, human.surname) && gender == human.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, name, surname, age, gender);
    }
}

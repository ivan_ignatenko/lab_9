import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class GenderComparator implements Comparator<Human> {

    @Override
    public int compare(@NotNull Human o1, @NotNull Human o2) {
        if(o1.getGender() == o2.getGender()){
            return 0;
        }
        else if(o1.getGender() == Human.Gender.M){
            return 1;
        }
        else{
            return -1;
        }
    }
}

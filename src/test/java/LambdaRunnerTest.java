import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

class LambdaRunnerTest {

    @Test
    void testStringLambdasRun() {
        Function<String, Integer> lambda = LambdaDemo.strLen;
        String s1 = "ffsd";

        assertEquals(4, LambdaRunner.stringLambdasRun(lambda, s1));

    }

    @Test
    void testHumanLambdasRun() throws HumanException {
        Function<Human, Integer> lambda = LambdaDemo.ageOfHuman;
        Human human = new Human("fsd", "bgfbf", "vdfd", 25, Human.Gender.M);

        assertEquals(25, LambdaRunner.HumanLambdasRun(lambda, human));
    }

    @Test
    void testHumanBiFuncRun() throws HumanException {
        BiFunction<Human, Human, Boolean> lambda = LambdaDemo.haveSameLastname;
        Human human = new Human("fs", "vcx", "jnh", 53, Human.Gender.M);
        Human human1 = new Human("fs", "mnjh", "hgt", 21, Human.Gender.F);

        assertTrue(LambdaRunner.humanBiFuncRun(lambda, human, human1));
    }

    @Test
    void testPersonPlusPlusRun() throws HumanException {
        Function<Human, Human> lambda = LambdaDemo.personPlusPlus;
        Human human = new Human("fsd", "bgfbf", "vdfd", 25, Human.Gender.M);

        assertEquals(26, LambdaRunner.personPlusPlusRun(lambda, human).getAge());
    }

    @Test
    void testCheckAgeRun() throws HumanException {
        TripleToOne lambda = LambdaDemo.checkAge;
        Human h1 = new Human("h", "z", "2", 42, Human.Gender.F);
        Human h2 = new Human("v", "c", "f", 17, Human.Gender.F);
        Human h3 = new Human("d", "s", "x", 30, Human.Gender.F);

        assertTrue(LambdaRunner.checkAgeRun(lambda, h1, h2, h3, 45));
    }
}